---
layout: archive
title: Documentation
excerpt: PP functional and technical documentation
tags: PP, evoting, opensource, release, documentation, requirements, architecture, design, release note, security, specifications
permalink: /doc/
image:
  feature: documents-banner.png
---

{% include toc.html %}

## Functional documentation
* [Documentation 1](https://www.google.com)
* [Documentation 2](https://www.google.com)
* [Documentation 3](https://www.google.com)


### General functional documentation

### Detailed functional requirements

#### Spécifications fonctionnelles Backoffice

<div class="documents cards">

{% include doc.html
   url="documentation/chvote-docs-functional/raw/master/BO/SFD-BO-Tableau%20de%20bord%20des%20opérations.pdf?inline=false"
   title="Tableau de bord des opérations"
%}

{% include doc.html
   url="documentation/chvote-docs-functional/raw/master/BO/SFD-BO-Page%20de%20pilotage.pdf?inline=false"
   title="Page de pilotage"
%}

{% include doc.html
   url="documentation/chvote-docs-functional/raw/master/BO/SFD-BO-Paramétrage-Opération.pdf?inline=false"
   title="Paramétrage opération"
%}

{% include doc.html
   url="documentation/chvote-docs-functional/raw/master/BO/SFD-BO-Paramétrage-Jalons-Opérationnels.pdf?inline=false"
   title="Paramétrage jalons opérationnels"
%}

{% include doc.html
   url="documentation/chvote-docs-functional/raw/master/BO/SFD-BO-Importer%20les%20référentiels%20de%20l'opération.pdf?inline=false"
   title="Importer les référentiels de l'opération"
%}

{% include doc.html
   url="documentation/chvote-docs-functional/raw/master/BO/SFD-BO-Importer%20le%20registre%20électoral.pdf?inline=false"
   title="Importer le registre électoral"
%}

{% include doc.html
   url="documentation/chvote-docs-functional/raw/master/BO/SFD-BO-Paramétrer%20le%20site%20de%20vote.pdf?inline=false"
   title="Paramétrer le site de vote"
%}

{% include doc.html
   url="documentation/chvote-docs-functional/raw/master/BO/SFD-BO-Paramétrage-Importer-Documentation.pdf?inline=false"
   title="Importer la documentation"
%}

{% include doc.html
   url="documentation/chvote-docs-functional/raw/master/BO/SFD-BO-Paramétrage-Importer-Documentation-Scrutin.pdf?inline=false"
   title="Importer la documentation pour un scrutin"
%}

{% include doc.html
   url="documentation/chvote-docs-functional/raw/master/BO/SFD-BO-Paramétrer%20l'affichage%20d'une%20élection.pdf?inline=false"
   title="Paramétrer l'affichage d'une élection"
%}

{% include doc.html
   url="documentation/chvote-docs-functional/raw/master/BO/SFD-BO-D%C3%A9finir%20la%20configuration%20imprimeur.pdf?inline=false"
   title="Définir la configuration imprimeur"
%}

{% include doc.html
   url="documentation/chvote-docs-functional/raw/master/BO/SFD-BO-Définir%20les%20cartes%20de%20vote%20de%20test%20et%20de%20contrôle.pdf?inline=false"
   title="Définir les cartes de vote de test et de contrôle"
%}

{% include doc.html
   url="documentation/chvote-docs-functional/raw/master/BO/SFD-BO-Inviter%20une%20entité%20de%20gestion.pdf?inline=false"
   title="Inviter une entité de gestion"
%}

{% include doc.html
   url="documentation/chvote-docs-functional/raw/master/BO/SFD-BO-Module%20d'administration.pdf?inline=false"
   title="Module d'administration"
%}
{% include doc.html
   url="documentation/chvote-docs-functional/raw/master/BO/SFD-BO-Tableau%20de%20bord%20Imprimeur.pdf?inline=false"
   title="Tableau de bord Imprimeur"
%}

</div>

<hr/>

## Design documentation

### Architecture

### Applications design

### Security

### Transversal design

### Software factory

## System interfaces contracts

## Development directives

