---
layout: archive
title: Demo
excerpt: Video demonstrating the usage of the CHVote applications to run a votation
tags: CHVote, evoting, opensource, demo, video, usage, votation
permalink: /demo/
image:
  feature: demo-banner.png
---
# More Jekyll Themes to consider
### [Jekyll Doc Theme 6.0](https://idratherbewriting.com/documentation-theme-jekyll/)
![](https://d1qmdf3vop2l07.cloudfront.net/enigmatic-tuba.cloudvent.net/compressed/_min_/3c4a6939681cfb3bd4c71229060b2827.webp)

---
### [Just the Docs](https://pmarsceill.github.io/just-the-docs/)
![](https://d1qmdf3vop2l07.cloudfront.net/enigmatic-tuba.cloudvent.net/compressed/_min_/034db23f8d9956c30865254f8226e203.webp
)

---
### [Minimal Mistakes](https://mmistakes.github.io/minimal-mistakes/)
![](https://d1qmdf3vop2l07.cloudfront.net/enigmatic-tuba.cloudvent.net/compressed/_min_/6611f009319054080e6784d4cf421af9.webp
)

---
### [Doks (29$)](https://doks.themejack.com/blue/)
![](https://d1qmdf3vop2l07.cloudfront.net/enigmatic-tuba.cloudvent.net/compressed/_min_/cfe0b0f4b7abc43578153a84669d310a.webp
)

---

All Jekyll themes can be found [here](http://jekyllthemes.org/) and [here](http://jekyllthemes.io/).

Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.

### Video in German

### Demo in English
[Link auf Webseite](https://google.com)
